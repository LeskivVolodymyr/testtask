﻿angular.module('app').component('items', {
    templateUrl: '/app/components/views/item.html',

    controller: function (itemService, $uibModal) {

        ctrl = this;
        ctrl.itemOnPage = 4;
        ctrl.maxSize = 4;
        ctrl.currentPage = 1;
        ctrl.items = [];
        getPageCount();
        ctrl.deleteRequest = false;
        ctrl.addRequest = false;

        ctrl.add = function (form) {
            if (form.$valid && !ctrl.addRequest) {
                ctrl.addRequest = true;
                itemService.add(ctrl.newItem).then(function (response) {
                    if (ctrl.currentPage == 1) {
                        if (ctrl.items.length == ctrl.itemOnPage) {
                            ctrl.items.splice(ctrl.itemOnPage-1, 1);
                        }
                        ctrl.items.unshift(response.data);
                    }
                    else {
                        ctrl.getItems();
                    }
                    if (ctrl.totalItems > 1 || ctrl.items.length == ctrl.itemOnPage) {
                        getPageCount();
                    }
                    ctrl.addRequest = false;
                }).catch(function () {
                    ctrl.addRequest = false;
                });
            }
        }

        ctrl.delete = function (id) {
            if (!ctrl.deleteRequest) {
                ctrl.deleteRequest = true;
                itemService.delete(id).then(function () {
                    getPageCount();
                    ctrl.getItems();
                    ctrl.deleteRequest = false;
                }).catch(function () {
                    ctrl.deleteRequest = false;
                });
            }

        }

        ctrl.edit = function (model) {
            var tempModel = angular.copy(model);
            var modalInstance = $uibModal.open({
                animation: true,
                component: 'editModal',
                resolve: {
                    item: function () {
                        return tempModel;
                    }
                }
            });

            modalInstance.result.then(function (isSuccess) {
                if (isSuccess) {
                    ctrl.getItems();
                }
            });
        };

        ctrl.getItems = function () {
            skip = (ctrl.currentPage - 1) * ctrl.itemOnPage;
            itemService.getItems(ctrl.itemOnPage, skip).then(function (response) {
                if (!response.data.length && ctrl.currentPage > 1) {
                    ctrl.getItems();
                }
                else {
                    ctrl.items = response.data;
                }

            });
        }
        ctrl.getItems();


        function getPageCount() {
            itemService.pageCount(ctrl.itemOnPage).then(function (response) {

                ctrl.totalItems = response.data * 10;
            });
        }


    }
})