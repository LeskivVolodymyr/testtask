﻿(function () {
    'use strict';

    function editModalController($uibModal, itemService) {
        var ctrl = this;
        ctrl.edit = function (form) {
            if (form.$valid) {
                itemService.edit(ctrl.resolve.item).then(function () {
                    ctrl.close({ $value: true });
                })
                    .catch(function () {
                        ctrl.close({ $value: false });
                    });
            }
        }

    }

    editModalController.$inject = ['$uibModal', 'itemService'];

    angular.module('app')
        .component('editModal', {
            templateUrl: '/app/components/views/editModal.html',
            controller: editModalController,
            bindings: {
                resolve: '<',
                close: '&'
            }
        });
})()
