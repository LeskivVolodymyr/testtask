﻿angular.module('app').component('statistic', {
    templateUrl: '/app/components/views/statistic.html',

    controller: function (itemService) {
        ctrl = this;
        ctrl.currentPage = 1;
        ctrl.maxSize = 4;
        ctrl.itemOnPage = 3;
        ctrl.items = [];
        getPageCount();

        ctrl.getItems = function () {
            skip = (ctrl.currentPage - 1) * ctrl.itemOnPage;
            itemService.getItemTypes(ctrl.itemOnPage, skip).then(function (response) {
                if (!response.data.length && ctrl.currentPage > 1) {
                    ctrl.getItems();
                }
                else {
                    ctrl.items = response.data;
                }
            });
        }
        ctrl.getItems();

        function getPageCount() {
            itemService.pageTypeCount(ctrl.itemOnPage).then(function (response) {
                ctrl.totalItems = response.data * 10;
            });
        }

    }
})