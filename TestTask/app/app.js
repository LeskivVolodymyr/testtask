﻿angular.module('app', ['ui.router',
    'ui.bootstrap',
    'item.service',
    'ngMessages',
    'toastr'

])
    .config(['$locationProvider', '$urlRouterProvider', '$httpProvider', '$stateProvider', function ($locationProvider, $urlRouterProvider, $httpProvider, $stateProvider) {

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

        $urlRouterProvider.otherwise('/items');
        $urlRouterProvider.when("/", "/items");

        var states = [
            {
                name: 'items',
                url: '/items',
                component: 'items'
            },
            {
                name: 'statistic',
                url: '/statistic',
                component: 'statistic'
            },
        ]

        states.forEach(function (state) {
            $stateProvider.state(state);
        });
    }]);

