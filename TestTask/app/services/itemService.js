﻿(function () {

    function itemService($http, toastr) {
        var factory = {};

        factory.add = function (model) {
            return $http.post("/api/v1/add", model)
                .catch(function (response) {
                    toastr.error('Something went wrong. Try again.', 'Error');
                });
        }

        factory.delete = function (id) {
            return $http.delete("/api/v1/delete?id=" + id)
                .catch(function (response) {
                    toastr.error('Something went wrong. Try again.', 'Error');
                });
        }

        factory.edit = function (model) {
            return $http.post("/api/v1/edit?id=", model)
                .catch(function (response) {
                    toastr.error('Something went wrong. Try again.', 'Error');
                });
        }

        factory.pageCount = function (count) {
            return $http.get("/api/v1/pageCount?itemsInPage=" + count);
        }

        factory.pageTypeCount = function (count) {
            return $http.get("/api/v1/pageTypeCount?itemsInPage=" + count);
        }

        factory.getItems = function (take, skip) {
            if (!skip)
                skip = 0;
            return $http.get("/api/v1/getItems?take=" + take + "&skip=" + skip);
        }

        factory.getItemTypes = function (take, skip) {
            if (!skip)
                skip = 0;
            return $http.get("/api/v1/getItemTypes?take=" + take + "&skip=" + skip);
        }

        return factory;
    }


    itemService.$inject = ['$http'];


    angular.module('item.service', [])
        .factory('itemService', itemService)

})();