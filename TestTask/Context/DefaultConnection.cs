﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TestTask.Models;

namespace TestTask.Context
{
    public class DefaultConnection : DbContext
    {
        public DbSet<ItemModel> Items { get; set; }
    }
}