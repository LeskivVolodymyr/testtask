﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TestTask.Context;
using TestTask.Utility;


namespace TestTask.Models
{
    public class DbInitializer : CreateDatabaseIfNotExists<DefaultConnection>
    {
        protected override void Seed(DefaultConnection db)
        {
            var Categories = new List<string> { "Mobile", "LapTop", "Pen", "Software" };
            var itemList = new List<ItemModel>();
            Random rnd = new Random();
            int count = rnd.Next(101, 301);

            for (var i = 0; i < count; i++)
            {
                var randomType = rnd.Next(0, 4);
                var item = new ItemModel
                {
                    Name = "Some item name " + (i + 1).ToString(),
                    TypeName = Categories[randomType],
                    DateCreate = UTCTimeUtility.GetLongTimeFromDate(DateTime.UtcNow) + i * i
                };
                itemList.Add(item);
            }
            db.Items.AddRange(itemList);

            base.Seed(db);
        }
    }
}