﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestTask.Utility
{
    public class UTCTimeUtility
    {
        public static long GetLongTimeFromDate(DateTime date)
        {
            return (long)TimeSpan.FromTicks(date.Ticks - new DateTime(1970, 1, 1).Ticks).TotalMilliseconds;
        }
    }
}