﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestTask.Context;
using TestTask.Models;
using TestTask.Utility;

namespace TestTask.Controllers
{
    [RoutePrefix("api/v1")]
    public class ItemController : ApiController
    {
        private DefaultConnection db = new DefaultConnection();

        [HttpPost]
        [Route("add")]
        public IHttpActionResult AddNewItem(ItemModel model)
        {
            if (String.IsNullOrEmpty(model.Name) || String.IsNullOrWhiteSpace(model.Name))
            {
                ModelState.AddModelError("Error:", "Name can not be empty.");
                return BadRequest(ModelState);
            }
            if (String.IsNullOrEmpty(model.TypeName) || String.IsNullOrWhiteSpace(model.TypeName))
            {
                ModelState.AddModelError("Error:", "Type can not be empty.");
                return BadRequest(ModelState);
            }

            model.DateCreate = UTCTimeUtility.GetLongTimeFromDate(DateTime.UtcNow);
            db.Items.Add(model);
            db.SaveChanges();
            return Ok(model);
        }


        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult DeleteItem(int id)
        {
            try
            {
                db.Items.Remove(db.Items.FirstOrDefault(x => x.Id == id));
                db.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("DataBaseError:", e.Message);
                return BadRequest(ModelState);
            }
        }


        [HttpPost]
        [Route("edit")]
        public IHttpActionResult EditItem(ItemModel model)
        {
            if (String.IsNullOrEmpty(model.Name) || String.IsNullOrWhiteSpace(model.Name))
            {
                ModelState.AddModelError("Error:", "Name can not be empty.");
                return BadRequest(ModelState);
            }
            if (String.IsNullOrEmpty(model.TypeName) || String.IsNullOrWhiteSpace(model.TypeName))
            {
                ModelState.AddModelError("Error:", "Type can not be empty.");
                return BadRequest(ModelState);
            }

            try
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("DataBaseError:", e.Message);
                return BadRequest(ModelState);
            }
        }


        [HttpGet]
        [Route("pageCount")]
        public IHttpActionResult PageCount(int itemsInPage)
        {
            var TotalCount = db.Items.Count();
            var result = Math.Ceiling((double)TotalCount / (double)itemsInPage);
            return Ok(result);
        }


        [HttpGet]
        [Route("getItems")]
        public IHttpActionResult GetItems(int take = 4, int skip = 0)
        {
            var result = db.Items.OrderByDescending(x => x.DateCreate).Skip(skip).Take(take).ToList();
            return Ok(result);
        }


        [HttpGet]
        [Route("pageTypeCount")]
        public IHttpActionResult PageTypeCount(int itemsInPage)
        {
            var TotalCount = db.Items.Select(x => x.TypeName).Distinct().Count();
            var result = Math.Ceiling((double)TotalCount / (double)itemsInPage);
            return Ok(result);
        }

        [HttpGet]
        [Route("getItemTypes")]
        public IHttpActionResult GetItemTypes(int take = 3, int skip = 0)
        {
            var result = db.Items.GroupBy(x => x.TypeName).Select(x => new
            {
                TypeName = x.Key,
                Count = x.Distinct().Count()
            }).ToList().Skip(skip).Take(take);

            return Ok(result);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}