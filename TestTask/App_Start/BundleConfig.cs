﻿using System.Web;
using System.Web.Optimization;

namespace TestTask
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/angular-toastr.css",
                      "~/Content/ui-bootstrap-custom-2.5.0-csp.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
               "~/Scripts/angular.min.js",
               "~/Scripts/angular-ui-router.min.js",
               "~/Scripts/ui-bootstrap-custom-2.5.0.min.js",
               "~/Scripts/ui-bootstrap-custom-tpls-2.5.0.min.js",
               "~/Scripts/angular-messages.js",
               "~/Scripts/angular-toastr.js",
               "~/Scripts/angular-toastr.tpls.js"
           ));



            bundles.Add(new ScriptBundle("~/bundles/angular-comp").Include(
                    "~/app/app.js",
                    "~/app/components/items.js",
                    "~/app/components/statistic.js",
                    "~/app/components/editModal.js",
                    "~/app/services/itemService.js"
                ));



        }
    }
}
